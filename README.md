# format-midi

Standard MIDI File Reader/Writer for Haxe Programming Language.

# Usage

```
#!haxe

// for read
var fileName = "test.mid"
var smfReader = new format.mid.Reader( sys.io.File.read( fileName ) );
var midi = smfReader.read( );

// for write
var fileName = "test2.mid"
var smfWriter = new format.mid.Writer( sys.io.file.write( fileName, true ) );
smfWriter.write( midi );

```

# Utility

format.mid.MIDIUtil is Utilitiy functions for generate/analyse SMF.

## convertBeatToMIDIBeat

convert for generate.

MIDI's denominator is exponent of power of two.

ex: music beat is 4/4, in MIDI beat it is numerator:4 and denominator:2.

## convertMIDIBeatToBeat

for analyse.

# Data Structures

these in format.mid.Data

## MIDI

### formatType

SMF format type

* 0 (major)
* 1
* 2 (rare)

 *Warning* i am not test on format type 2. because i don't have format type 2 SMF.

### trackCount

track count.

it is not need fill value in order to use format.mid.Writer.

### timebase

time base.

### tracks

all track data.

## MIDITrack

### trackNumber

track number.

it is not need fill value in order to use format.mid.Writer.

### events

track's events

## MIDIEvent

### time

it is *absolute time*, not delta time.

### channelNumber

channel number

### type

MIDI event type.

# License

MIT License