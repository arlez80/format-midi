package format.mid;

import format.mid.Data;
import haxe.io.BytesInput;
import haxe.io.Input;
import haxe.io.Eof;

typedef SMFChunk = {
	id:String,
	bi:BytesInput,
};

/**
 * MIDI Reader
 * @author あるる（きのもと 結衣）
 */
class Reader
{
	/// データソース
	private var data:Input;
	/// 最後に読み取ったイベント
	private var lastEventType:Int;

	/**
	 * コンストラクタ
	 * @param	data
	 */
	public function new( data:Input ) 
	{
		this.data = data;
		this.data.bigEndian = true;
	}

	/**
	 * 可変バイト数字読み込み
	 * @return
	 */
	inline private function readVariableInt( bi:Input ):Int
	{
		var result:Int = 0;

		while ( true ) {
			var c = bi.readByte( );
			if ( ( c & 0x80 ) != 0 ) {
				result |= c & 0x7f;
				result <<= 7;
			}else {
				result |= c;
				break;
			}
		}

		return result;
	}

	/**
	 * チャンクデータ読み込み
	 * @param	bi
	 * @return	チャンクデータを返す
	 */
	inline private function readChunkData( bi:Input ):SMFChunk
	{
		var id = bi.readString( 4 );
		var size = bi.readInt32( );
		var bi = new BytesInput( bi.read( size ) );
		bi.bigEndian = true;

		return {
			id: id,
			bi: bi
		};
	}

	/**
	 * 読み込み
	 * @return
	 */
	public function read( ):MIDI
	{
		// ヘッダ
		var header = this.readChunkData( this.data );
		if ( ( header.id != "MThd" ) && ( header.bi.length != 6 ) ) {
			throw "MThd header expected";
		}
		// フォーマットタイプ
		var formatType = header.bi.readUInt16( );
		// トラック数
		var trackCount = header.bi.readUInt16( );
		// 分解能
		var timebase = header.bi.readUInt16( );

		// トラック読み込み
		var tracks = new Array<MIDITrack>( );
		for ( i in 0 ... trackCount ) {
			tracks.push( this.readTrack( i ) );
		}

		return {
			formatType: formatType,
			trackCount: trackCount,
			timebase: timebase,
			tracks: tracks,
		};
	}

	/**
	 * トラック読み込み
	 * @return
	 */
	private function readTrack( trackNumber:Int ):MIDITrack
	{
		var trackChunk = this.readChunkData( this.data );
		if ( trackChunk.id != "MTrk" ) {
			throw "unknown chunk: " + trackChunk.id;
		}
		var bi = trackChunk.bi;
		var time:Int = 0;
		var events = new Array<MIDIEvent>( );

		try {
			while ( true ) {
				var deltaTime = this.readVariableInt( bi );
				time += deltaTime;
				var eventTypeByte = bi.readByte( );

				var type:MIDIEventType = null;
				if ( this.isSystemEvent( eventTypeByte ) ) {
					type = MIDIEventType.systemEvent( this.readSystemEvent( bi, eventTypeByte ) );
				}else {
					type = this.readEvent( bi, eventTypeByte );
					if( ( eventTypeByte & 0x80 ) == 0 ) {
						eventTypeByte = this.lastEventType;
					}
				}
	
				events.push( {
					time: time,
					channelNumber: eventTypeByte & 0x0f,
					type: type,
				} );
			}
		}catch( eof:Eof ) {
			// 終了
		}

		return {
			trackNumber: trackNumber,
			events: events,
		};
	}

	/**
	 * システムイベントか？
	 * @param	b
	 * @return
	 */
	inline private function isSystemEvent( b:Int ):Bool
	{
		return ( b & 0xf0 == 0xf0 );
	}

	/**
	 * システムイベント読み込み
	 * @param	eventType
	 */
	private function readSystemEvent( bi:Input, eventTypeByte:Int ):MIDISystemEvent
	{
		if ( eventTypeByte == 0xff ) {
			var metaType = bi.readByte( );
			var size = this.readVariableInt( bi );

			return switch( metaType ) {
				case 0x01: MIDISystemEvent.textEvent( bi.readString( size ) );
				case 0x02: MIDISystemEvent.copyright( bi.readString( size ) );
				case 0x03: MIDISystemEvent.trackName( bi.readString( size ) );
				case 0x04: MIDISystemEvent.instrumentName( bi.readString( size ) );
				case 0x05: MIDISystemEvent.lyric( bi.readString( size ) );
				case 0x06: MIDISystemEvent.marker( bi.readString( size ) );
				case 0x07: MIDISystemEvent.cuePoint( bi.readString( size ) );
				case 0x20:
					if ( size != 1 ) throw "MIDI Channel Prefix length is not 1.";
					MIDISystemEvent.MIDIChannelPrefix( bi.readByte( ) );
				case 0x2f:
					if ( size != 0 ) throw "end of track with unknown data.";
					MIDISystemEvent.endOfTrack;
				case 0x51:
					if ( size != 3 ) throw "tempo length is not 3.";
					var mpb = bi.readByte() << 16;
					mpb |= bi.readByte() << 8;
					mpb |= bi.readByte();
					MIDISystemEvent.setTempo( mpb );
				case 0x54:
					if ( size != 5 ) throw "SMPTE length is not 5.";
					var hr = bi.readByte( );
					var mm = bi.readByte( );
					var se = bi.readByte( );
					var fr = bi.readByte( );
					var ff = bi.readByte( );
					MIDISystemEvent.smpteOffset( hr, mm, se, fr, ff );
				case 0x58:
					if ( size != 4 ) throw "beat length is not 4.";
					var numerator = bi.readByte( );
					var denominator = bi.readByte( );
					var clock = bi.readByte( );
					var beat32 = bi.readByte( );
					MIDISystemEvent.beat( numerator, denominator, clock, beat32 );
				case 0x59:
					if ( size != 2 ) throw "beat length is not 2.";
					var sf = bi.readInt8( );
					var minor = bi.readByte( ) == 1;
					MIDISystemEvent.key( sf, minor );
				default:
					MIDISystemEvent.unknown( metaType, bi.read( size ) );
			}
		}else if ( eventTypeByte == 0xf0 ) {
			var size = this.readVariableInt( bi );
			return MIDISystemEvent.sysEx( bi.read( size ) );
		}else if ( eventTypeByte == 0xf7 ) {
			var size = this.readVariableInt( bi );
			return MIDISystemEvent.dividedSysEx( bi.read( size ) );
		}

		throw "unknown system event type:" + eventTypeByte;
	}

	/**
	 * 通常イベント読み込み
	 * @return
	 */
	private function readEvent( bi:Input, eventTypeByte:Int ):MIDIEventType
	{
		var param:Int = 0;
		if ( ( eventTypeByte & 0x80 ) == 0 ) {
			param = eventTypeByte;
			eventTypeByte = this.lastEventType;
		}else {
			param = bi.readByte( );
			this.lastEventType = eventTypeByte;
		}
		var eventType = eventTypeByte & 0xf0;

		return switch( eventType ) {
			case 0x80:
				MIDIEventType.noteOff( param, bi.readByte( ) );
			case 0x90:
				MIDIEventType.noteOn( param, bi.readByte( ) );
			case 0xa0:
				MIDIEventType.polyphonicKeyPressure( param, bi.readByte( ) );
			case 0xb0:
				MIDIEventType.controlChange( cast param, bi.readByte( ) );
			case 0xc0:
				MIDIEventType.programChange( param );
			case 0xd0:
				MIDIEventType.channelPressure( param );
			case 0xe0:
				MIDIEventType.pitchBend( param | ( bi.readByte( ) << 7 ) );
			default:
				throw "unknown event type:" + eventTypeByte;
		};
	}
}