package format.mid;

import format.mid.Data.MIDI;
import haxe.io.Bytes;

typedef MIDI = {
	formatType:Int,
	trackCount:Int,
	timebase:Int,
	tracks:Array<MIDITrack>,
};

typedef MIDITrack = {
	trackNumber:Int,
	events:Array<MIDIEvent>,
};

typedef MIDIEvent = {
	time:Int,
	channelNumber:Int,
	type:MIDIEventType,
};

enum MIDIEventType
{
	// 8*
	noteOff( noteNumber:Int, velocity:Int );
	// 9*
	noteOn( noteNumber:Int, velocity:Int );
	// a*
	polyphonicKeyPressure( noteNumber:Int, value:Int );
	// b*
	controlChange( controlNumber:MIDIControlNumber, value:Int );
	// c*
	programChange( programNumber:Int );
	// d*
	channelPressure( value:Int );
	// e*
	pitchBend( value:Int );
	// f*
	systemEvent( event:MIDISystemEvent );
}

enum MIDISystemEvent
{
	sysEx( data:Bytes );
	dividedSysEx( data:Bytes );

	// 01
	textEvent( text:String );
	// 02
	copyright( terms:String );
	// 03
	trackName( name:String );
	// 04
	instrumentName( text:String );
	// 05
	lyric( text:String );
	// 06
	marker( text:String );
	// 07
	cuePoint( text:String );
	// 20
	MIDIChannelPrefix( cc:Int );
	// 2f
	endOfTrack;

	// 51
	setTempo( mpb:Int );

	// 54
	smpteOffset( hr:Int, mm:Int, se:Int, fr:Int, ff:Int );
	// 58
	beat( numerator:Int, denominator:Int, clock:Int, beat32:Int );
	// 59
	key( sf:Int, minor:Bool );

	// others ...
	unknown( metaType:Int, bi:Bytes );
}

@:enum
abstract MIDIControlNumber(Int)
{
	var bankSelectMSB		= 0x00;
	var modulation			= 0x01;
	var breathController	= 0x02;
	var footController		= 0x04;
	var portamentoTime		= 0x05;
	var dataEntry			= 0x06;
	var channelVolume		= 0x07;
	var balance				= 0x08;
	var pan					= 0x0a;
	var expression			= 0x0b;

	var bankSelectLSB		= 0x20;
	var modulationLSB		= 0x21;
	var breathControllerLSB	= 0x22;
	var footControllerLSB	= 0x24;
	var portamentoTimeLSB	= 0x25;
	var dataEntryLSB		= 0x26;
	var channelVolumeLSB	= 0x27;
	var balanceLSB			= 0x28;
	var panLSB				= 0x2a;
	var expressionLSB		= 0x2b;

	var hold				= 0x40;
	var portamento			= 0x41;
	var sostenuto			= 0x42;
	var softPedal			= 0x43;
	var legatoFootSwitch	= 0x44;
	var freeze				= 0x45;
	var soundVariation		= 0x46;
	var timbre				= 0x47;
	var releaseTime			= 0x48;
	var attackTime			= 0x49;
	var brightness			= 0x4a;
	var vibratoRate			= 0x4b;
	var vibratoDepth		= 0x4c;
	var vibratoDelay		= 0x4d;


	var nrpnLSB				= 0x62;
	var nrpnMSB				= 0x63;
	var rpnLSB				= 0x64;
	var rpnMSB				= 0x65;
}

@:enum
abstract MIDIManufactureID(Int)
{
	var universalNonRealTimeSysEx				= 0x7e;
	var universalRealTimeSysEx					= 0x7f;

	var KAWAI_MUSICAL_INSTRUMENTS_MFG_CO_LTD	= 0x40;
	var ROLAND_CORPORATION						= 0x41;
	var KORG_INC								= 0x42;
	var YAMAHA_CORPORATION						= 0x43;
	var CASIO_COMPUTER_CO_LTD					= 0x44;
	var KAMIYA_STUDIO_CO_LTD					= 0x46;
	var AKAI_ELECTRIC_CO_LTD					= 0x47;
}
