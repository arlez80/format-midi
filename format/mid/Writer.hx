package format.mid;

import format.mid.Data;
import haxe.io.BytesOutput;
import haxe.io.Output;

/**
 * MIDI Writer
 * @author あるる（きのもと 結衣）
 */
class Writer
{
	private var o:Output;
	private var midi:MIDI;

	/**
	 * コンストラクタ
	 * @param	output
	 */
	public function new( output:Output )
	{
		this.o = output;
		o.bigEndian = true;
	}

	/**
	 * 可変バイト数字書き込み
	 * @param	i	数字
	 */
	inline private function writeVariableInt( bo:Output, i:Int ):Void
	{
		if ( i < ( 1 << 7 ) ) {
			bo.writeByte( i );
		}else if ( i <= ( 1 << 14 ) ) {
			bo.writeByte( ( ( i >> 7 ) & 0x7f ) | 0x80 );
			bo.writeByte( i & 0x7f );
		}else if ( i <= ( 1 << 28 ) ) {
			bo.writeByte( ( ( i >> 14 ) & 0x7f ) | 0x80 );
			bo.writeByte( ( ( i >> 7 ) & 0x7f ) | 0x80 );
			bo.writeByte( i & 0x7f );
		}else {
			bo.writeByte( ( ( i >> 28 ) & 0x7f ) | 0x80 );
			bo.writeByte( ( ( i >> 14 ) & 0x7f ) | 0x80 );
			bo.writeByte( ( ( i >> 7 ) & 0x7f ) | 0x80 );
			bo.writeByte( i & 0x7f );
		}
	}

	/**
	 * 書き出し
	 * @param	mid
	 */
	public function write( midi:MIDI ):Void
	{
		this.midi = midi;

		// ヘッダ
		this.o.writeString( "MThd" );
		this.o.writeInt32( 6 );
		this.o.writeUInt16( midi.formatType );
		this.o.writeUInt16( midi.tracks.length );
		this.o.writeUInt16( midi.timebase );

		// トラック
		for ( t in midi.tracks ) {
			this.writeTrack( t );
		}
	}

	/**
	 * トラック書き出し
	 * @param	track
	 */
	private function writeTrack( track:MIDITrack ):Void
	{
		// 時系列順にならべる
		var events = track.events.concat( [] );
		events.sort( function( a, b ) {
			return ( a.time < b.time ) ? -1 : 1;
		} );

		// イベント出力
		var bo = new BytesOutput( );
		bo.bigEndian = true;

		var time:Int = 0;
		// var lastEventType:Int = 0;	// TODO
		for ( e in events ) {
			this.writeVariableInt( bo, e.time - time );
			time = e.time;

			switch( e.type ) {
				case MIDIEventType.noteOff( note, velocity ):
					bo.writeByte( 0x80 | e.channelNumber );
					bo.writeByte( note );
					bo.writeByte( velocity );
				case MIDIEventType.noteOn( note, velocity ):
					bo.writeByte( 0x90 | e.channelNumber );
					bo.writeByte( note );
					bo.writeByte( velocity );
				case MIDIEventType.polyphonicKeyPressure( note, value ):
					bo.writeByte( 0xa0 | e.channelNumber );
					bo.writeByte( note );
					bo.writeByte( value );
				case MIDIEventType.controlChange( controlNumber, value ):
					bo.writeByte( 0xb0 | e.channelNumber );
					bo.writeByte( cast controlNumber );
					bo.writeByte( value );
				case MIDIEventType.programChange( programNumber ):
					bo.writeByte( 0xc0 | e.channelNumber );
					bo.writeByte( programNumber );
				case MIDIEventType.channelPressure( value ):
					bo.writeByte( 0xd0 | e.channelNumber );
					bo.writeByte( value );
				case MIDIEventType.pitchBend( value ):
					bo.writeByte( 0xe0 | e.channelNumber );
					bo.writeByte( value & 0x7f );
					bo.writeByte( ( value >> 7 ) & 0x7f );
				case MIDIEventType.systemEvent( event ):
					this.writeSystemEvent( bo, event );
			}
		}

		// 書き込み
		this.o.writeString( "MTrk" );
		this.o.writeInt32( bo.length );
		this.o.write( bo.getBytes( ) );
	}

	/**
	 * システムイベント書き込み
	 * @param	event
	 */
	private function writeSystemEvent( bo:Output, event:MIDISystemEvent ):Void
	{
		switch( event ) {
			case MIDISystemEvent.sysEx( data ):
				bo.writeByte( 0xf0 );
				this.writeVariableInt( bo, data.length );
				bo.write( data );
			case MIDISystemEvent.dividedSysEx( data ):
				bo.writeByte( 0xf7 );
				this.writeVariableInt( bo, data.length );
				bo.write( data );

			case MIDISystemEvent.textEvent( text ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x01 );
				this.writeVariableInt( bo, text.length );
				bo.writeString( text );
			case MIDISystemEvent.copyright( text ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x02 );
				this.writeVariableInt( bo, text.length );
				bo.writeString( text );
			case MIDISystemEvent.trackName( text ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x03 );
				this.writeVariableInt( bo, text.length );
				bo.writeString( text );
			case MIDISystemEvent.instrumentName( text ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x04 );
				this.writeVariableInt( bo, text.length );
				bo.writeString( text );
			case MIDISystemEvent.lyric( text ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x05 );
				this.writeVariableInt( bo, text.length );
				bo.writeString( text );
			case MIDISystemEvent.marker( text ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x06 );
				this.writeVariableInt( bo, text.length );
				bo.writeString( text );
			case MIDISystemEvent.cuePoint( text ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x07 );
				this.writeVariableInt( bo, text.length );
				bo.writeString( text );

			case MIDISystemEvent.MIDIChannelPrefix( cc ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x20 );
				bo.writeByte( 0x01 );
				bo.writeByte( cc );
			case MIDISystemEvent.endOfTrack:
				bo.writeByte( 0xff );
				bo.writeByte( 0x2f );
				bo.writeByte( 0x00 );

			case MIDISystemEvent.setTempo( mpb ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x51 );
				bo.writeByte( 0x03 );
				bo.writeByte( ( mpb >> 16 ) & 0xff );
				bo.writeByte( ( mpb >> 8 ) & 0xff );
				bo.writeByte( mpb & 0xff );

			case MIDISystemEvent.smpteOffset( hr, mm, se, fr, ff ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x54 );
				bo.writeByte( 0x05 );
				bo.writeByte( hr );
				bo.writeByte( mm );
				bo.writeByte( se );
				bo.writeByte( fr );
				bo.writeByte( ff );

			case MIDISystemEvent.beat( numerator, denominator, clock, beat32 ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x58 );
				bo.writeByte( 0x04 );
				bo.writeByte( numerator );
				bo.writeByte( denominator );
				bo.writeByte( clock );
				bo.writeByte( beat32 );

			case MIDISystemEvent.key( sf, minor ):
				bo.writeByte( 0xff );
				bo.writeByte( 0x59 );
				bo.writeByte( 0x02 );
				bo.writeInt8( sf );
				bo.writeByte( minor ? 1 : 0 );

			case MIDISystemEvent.unknown( metaType, b ):
				bo.writeByte( 0xff );
				bo.writeByte( metaType );
				bo.writeByte( b.length );
				bo.write( b );
		}
	}
}
