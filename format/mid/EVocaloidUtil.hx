package format.mid;

import format.mid.Data;
import haxe.io.BytesOutput;

/**
 * EVocaloid MIDI Messages Utility
 * @author あるる（きのもと 結衣）
 */
class EVocaloidUtil
{
	static private var charCodeTable:Array<{char:String, code:Int}> = [{char:"あ", code:0}, {char:"い", code:1}, {char:"う", code:2}, {char:"え", code:3}, {char:"お", code:4}, {char:"か", code:5}, {char:"き", code:6}, {char:"く", code:7}, {char:"け", code:8}, {char:"こ", code:9}, {char:"が", code:10}, {char:"ぎ", code:11}, {char:"ぐ", code:12}, {char:"げ", code:13}, {char:"ご", code:14}, {char:"きゃ", code:15}, {char:"きゅ", code:16}, {char:"きょ", code:17}, {char:"ぎゃ", code:18}, {char:"ぎゅ", code:19}, {char:"ぎょ", code:20}, {char:"さ", code:21}, {char:"すぃ", code:22}, {char:"す", code:23}, {char:"せ", code:24}, {char:"そ", code:25}, {char:"ざ", code:26}, {char:"ずぃ", code:27}, {char:"ず", code:28}, {char:"ぜ", code:29}, {char:"ぞ", code:30}, {char:"しゃ", code:31}, {char:"し", code:32}, {char:"しゅ", code:33}, {char:"しぇ", code:34}, {char:"しょ", code:35}, {char:"じゃ", code:36}, {char:"じ", code:37}, {char:"じゅ", code:38}, {char:"じぇ", code:39}, {char:"じょ", code:40}, {char:"た", code:41}, {char:"てぃ", code:42}, {char:"とぅ", code:43}, {char:"て", code:44}, {char:"と", code:45}, {char:"だ", code:46}, {char:"でぃ", code:47}, {char:"どぅ", code:48}, {char:"で", code:49}, {char:"ど", code:50}, {char:"てゅ", code:51}, {char:"でゅ", code:52}, {char:"ちゃ", code:53}, {char:"ち", code:54}, {char:"ちゅ", code:55}, {char:"ちぇ", code:56}, {char:"ちょ", code:57}, {char:"つぁ", code:58}, {char:"つぃ", code:59}, {char:"つ", code:60}, {char:"つぇ", code:61}, {char:"つぉ", code:62}, {char:"な", code:63}, {char:"に", code:64}, {char:"ぬ", code:65}, {char:"ね", code:66}, {char:"の", code:67}, {char:"にゃ", code:68}, {char:"にゅ", code:69}, {char:"にょ", code:70}, {char:"は", code:71}, {char:"ひ", code:72}, {char:"ふ", code:73}, {char:"へ", code:74}, {char:"ほ", code:75}, {char:"ば", code:76}, {char:"び", code:77}, {char:"ぶ", code:78}, {char:"べ", code:79}, {char:"ぼ", code:80}, {char:"ぱ", code:81}, {char:"ぴ", code:82}, {char:"ぷ", code:83}, {char:"ぺ", code:84}, {char:"ぽ", code:85}, {char:"ひゃ", code:86}, {char:"ひゅ", code:87}, {char:"ひょ", code:88}, {char:"びゃ", code:89}, {char:"びゅ", code:90}, {char:"びょ", code:91}, {char:"ぴゃ", code:92}, {char:"ぴゅ", code:93}, {char:"ぴょ", code:94}, {char:"ふぁ", code:95}, {char:"ふぃ", code:96}, {char:"ふゅ", code:97}, {char:"ふぇ", code:98}, {char:"ふぉ", code:99}, {char:"ま", code:100}, {char:"み", code:101}, {char:"む", code:102}, {char:"め", code:103}, {char:"も", code:104}, {char:"みゃ", code:105}, {char:"みゅ", code:106}, {char:"みょ", code:107}, {char:"や", code:108}, {char:"ゆ", code:109}, {char:"よ", code:110}, {char:"ら", code:111}, {char:"り", code:112}, {char:"る", code:113}, {char:"れ", code:114}, {char:"ろ", code:115}, {char:"りゃ", code:116}, {char:"りゅ", code:117}, {char:"りょ", code:118}, {char:"わ", code:119}, {char:"ゐ", code:120}, {char:"ゑ", code:121}, {char:"を", code:122}, {char:"N\\", code:123}, {char:"m", code:124}, {char:"N", code:125}, {char:"J", code:126}, {char:"n", code:127}, {char:"づぁ", code:26}, {char:"づぃ", code:27}, {char:"づ", code:28}, {char:"づぇ", code:29}, {char:"づぉ", code:30}, {char:"うぃ", code:120}, {char:"うぇ", code:121}, {char:"うぉ", code:122}, {char:"ん", code:123}, {char:"っ", code:128}, {char:"、", code:128}, {char:"。", code:128}, {char:"ー", code:255}];

	inline static public function makeSysEx( ):BytesOutput
	{
		var o = new BytesOutput( );
		for ( t in [ 0x43, 0x79, 0x09, 0x11 ] ) o.writeByte( t );
		return o;
	}

	/**
	 * 歌詞に変換する
	 * @param	lyric
	 * @param	slot
	 * @return
	 */
	static public function toLyric( lyric:String, slot:Int = 0 ):MIDISystemEvent
	{
		var o = EVocaloidUtil.makeSysEx( );
		o.writeByte( 0x0a );
		o.writeByte( slot & 0x0f );
		var lyricChar = lyric.split( "" );
		while ( 0 < lyricChar.length ) {
			var c = lyricChar.shift( );
			var found = false;

			if( 2 <= lyricChar.length ) {
				var c2 = c + lyricChar[0];
				for ( t in charCodeTable ) {
					if ( c2 == t.char ) {
						o.writeByte( t.code );
						found = true;
						break;
					}
				}
			}
			if ( ! found ) {
				for ( t in charCodeTable ) {
					if ( c == t.char ) {
						o.writeByte( t.code );
						break;
					}
				}
			}
		}
		return MIDISystemEvent.sysEx( o.getBytes( ) );
	}

	/**
	 * 歌詞変換：1文字のみ変換する
	 * @param	lyric
	 * @param	slot
	 * @return
	 */
	static public function toLyricChar( lyric:String, slot:Int = 0 ):MIDISystemEvent
	{
		var o = EVocaloidUtil.makeSysEx( );
		o.writeByte( 0x0a );
		o.writeByte( slot & 0x0f );
		for ( t in charCodeTable ) {
			if ( lyric == t.char ) {
				o.writeByte( t.code );
				break;
			}
		}
		return MIDISystemEvent.sysEx( o.getBytes( ) );
	}
}
