package format.mid;

import format.mid.Data;
import haxe.io.BytesOutput;

typedef Beat = {
	numerator:Int,
	denominator:Int,
};

/**
 * MIDI Messages Utility
 * @author あるる（きのもと 結衣）
 */
class MIDIUtil
{
	static public var GMReset( get, never ):MIDISystemEvent;
	static public var XGSystemOn( get, never ):MIDISystemEvent;
	static public var GSReset( get, never ):MIDISystemEvent;

	static private function get_GMReset( ):MIDISystemEvent
	{
		var o = new BytesOutput( );
		o.writeByte( 0x7e );
		o.writeByte( 0x7f );
		o.writeByte( 0x09 );
		o.writeByte( 0x01 );
		o.writeByte( 0x7f );
		return MIDISystemEvent.sysEx( o.getBytes( ) );
	}

	static private function get_XGSystemOn( ):MIDISystemEvent
	{
		var o = new BytesOutput( );
		o.writeByte( 0x43 );
		o.writeByte( 0x10 );
		o.writeByte( 0x4c );
		o.writeByte( 0x00 );
		o.writeByte( 0x00 );
		o.writeByte( 0x7e );
		o.writeByte( 0x00 );
		o.writeByte( 0x7f );
		return MIDISystemEvent.sysEx( o.getBytes( ) );
	}

	static private function get_GSReset( ):MIDISystemEvent
	{
		var o = new BytesOutput( );
		o.writeByte( 0x41 );
		o.writeByte( 0x10 );
		o.writeByte( 0x42 );
		o.writeByte( 0x12 );
		o.writeByte( 0x40 );
		o.writeByte( 0x00 );
		o.writeByte( 0x7f );
		o.writeByte( 0x00 );
		o.writeByte( 0x41 );
		o.writeByte( 0x7f );
		return MIDISystemEvent.sysEx( o.getBytes( ) );
	}

	static public function convertBeatToMIDIBeat( beat:Beat ):MIDISystemEvent
	{
		var n:Int = beat.numerator;
		var d:Int = 0;
		var c:Int = 0x18;
		var b32:Int = 0x08;

		var de = beat.denominator;
		while ( 0 < de ) {
			d ++;
			de >>= 1;
		}

		return MIDISystemEvent.beat( n, d, c, b32 );
	}

	static public function convertMIDIBeatToBeat( m:MIDISystemEvent ):Beat
	{
		switch( m ) {
			case MIDISystemEvent.beat( n, d, c, b32 ):
				return { numerator: n, denominator: 1 << d };
			default:
				throw "it is not beat data";
		};
	}

	static public function parseChannelsFromTrack( track:MIDITrack ):Array<Array<MIDIEvent>>
	{
		var channels = new Array<Array<MIDIEvent>>( );
		for ( i in 0 ... 16 ) channels.push( new Array<MIDIEvent>( ) );

		for ( e in track.events ) {
			channels[e.channelNumber].push( e );
		}

		return channels;
	}
}
