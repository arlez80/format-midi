package;

import haxe.io.BytesInput;
import neko.Lib;
import sys.io.File;
import format.mid.*;

/**
 * ...
 * @author あるる（きのもと 結衣）
 */
class Main 
{
	
	static function main() 
	{
		var smfReader = new format.mid.Reader( File.read( "cr_map.mid" ) );
		var mid = smfReader.read( );

		trace( mid );

		var smfWriter = new format.mid.Writer( File.write( "cr_map2.mid", true ) );
		smfWriter.write( mid );

		trace( "end" );

		EVocaloidUtil.toLyric( "こんにちは", 0 );
	}
	
}
